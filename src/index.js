import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'
import App from './pages/App.vue';
import * as appRouter from './app-router';
import './assets/styles/_index.scss';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

const app = createApp(App)
app.use(appRouter.router);
app.mount('#app');
app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
