import { computed, reactive } from 'vue';
import { jwtDecrypt, tokenAlive } from '@/utils/jwt-helper';
import { AuthData } from '@/abstracts/auth-data';
import { AuthState } from '@/abstracts/auth-state';
import { LoginData } from '@/abstracts/login-data';
import { ApiRequest } from '@/models/api-request';
import { RegisterData } from '@/abstracts/register-data';

const apiRequest = new ApiRequest();

const authState = reactive<AuthState>({
    authData: {
        token: '',
        tokenExp: 0,
    },
    loginStatus: ''
});

export function useAuthStore() {
    const setAuthData = (authData: AuthData) => {
        authState.authData = authData;
    }

    const setLoginStatus = (loginStatus: string) => {
        authState.loginStatus = loginStatus
    }

    const authData = computed(() => authState.authData);

    const loginStatus = computed(() => authState.loginStatus);

    const isTokenAlive = () => {
        return !authState.authData.tokenExp ? false : tokenAlive(authState.authData.tokenExp);
    }

    const login = async (data: LoginData) => {
        const response = await apiRequest.post<LoginData>('auth/login/', data);

        if (response && response.data) {
            setLoginStatus('success');
            saveTokenData(response.data.accessToken);

            window.location.href = '/main';

            return;
        }

        setLoginStatus('failed');
    }

    const register = async (data: RegisterData) => {
        const response = await apiRequest.post<RegisterData>('auth/registration/', data);

        if (response && response.data) {
            saveTokenData(response.data.accessToken);

            window.location.href = '/';
        }

        return;
    };

    const saveTokenData = (accessToken: any) => {
        localStorage.setItem('access_token', accessToken);

        const jwtDecryption = jwtDecrypt(accessToken);

        authState.authData = {
            token: accessToken,
            tokenExp: jwtDecryption?.exp
        };
    }

    return {
        saveTokenData,
        login,
        register,
        isTokenAlive,
        loginStatus,
        authData,
        setLoginStatus,
        setAuthData
    }
}