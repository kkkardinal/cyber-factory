import { computed, reactive } from 'vue';

const authMenuState = reactive({
    isEntry: true
});

export function useHeaderStore() {
    const setEntryStatus = (isEntry: boolean) => {
        authMenuState.isEntry = isEntry;
    };

    const isEntry = computed(() => authMenuState.isEntry);

    const changeEntryStatus = (status: boolean) => {
        setEntryStatus(status);
    };

    return {
        setEntryStatus,
        isEntry,
        changeEntryStatus
    }
}
