import { computed, ref } from 'vue';
import { ApiRequest } from '@/models/api-request';
import {Cps} from "@/abstracts/cps";

const api = new ApiRequest();

const CpsStore = ref<Cps[]>([]);

export function useCpsStore() {
    const setCpss = (kfss: Cps[]) => {
        CpsStore.value = kfss
    };

    const cpss = computed(() => CpsStore);

    const getCpss = async () => {
        const cpss = await api.get('cps');
        setCpss(cpss.data.items);
    };

    const createCps = async (cps: any) => {
        await api.post('cps', cps);
    };

    const updateCps = async (id: number, cps: any) => {
        await api.put(`cps/${id} `, cps);
    };

    const deleteCps = async(id: number) => {
        await api.delete(`cps/${id}`);
    };

    return {
        cpss,
        setCpss,
        getCpss,
        createCps,
        updateCps,
        deleteCps
    }
}