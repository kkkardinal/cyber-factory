import {ApiRequest} from "@/models/api-request";

const api = new ApiRequest();

export function useDeviceStore() {

    const createDevice = async (device: any) => {
        await api.post('devices', device);
    };

    const updateDevice = async (device: any) => {
        await api.put(`devices/${device.id} `, device);
    };

    const deleteDevice = async(id: number) => {
        await api.delete(`devices/${id}`);
    };

    return {
        createDevice,
        updateDevice,
        deleteDevice
    }
}