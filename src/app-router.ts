import { createRouter, createWebHistory } from "vue-router";
import Home from "./pages/Home.vue";
import AuthPage from "@/pages/AuthPage.vue";
import {useAuthStore} from "@/store/use-auth-store";
import {ApiRequest} from "@/models/api-request";
import ErrorPage from "@/pages/ErrorPage.vue";

const routes = [
  { path: "/main", component: Home, meta: { requiredAuth: false } },
  { path: "/eww", component: Home, meta: { requiredAuth: false } },
  { path: "/", component: AuthPage, meta: { requiredAuth: false } },
  {
    name: 'Error page',
    path: '/:catchAll(.*)',
    component: ErrorPage,
    meta: {
      requiredAuth: false
    }
  }
];

const apiRequest = new ApiRequest();

export const router = createRouter({
  history: createWebHistory(),
  routes
});

const {
  authData,
  saveTokenData,
  isTokenAlive
} = useAuthStore();

router.beforeEach(async (to: any, from: any, next: any) => {
  console.log(authData.value.token)
  if (!authData.value.token) {
    getTokens();
  }

  let auth = isTokenAlive();

  if (!auth && authData.value.token) {
    const refreshResponse = await apiRequest.get('auth/refresh');
    saveTokenData(refreshResponse.data.accessToken);

    auth = true;
  }

  if (to.fullPath == '/') {
    return next();
  }

  if (auth && !to.meta.requiredAuth && to.path !== '/main') {
    return next('/main');
  }

  if (!auth && to.meta.requiredAuth) {
    return next('/auth');
  }

  return next();
});

const getTokens = () => {
  const access_token = localStorage.getItem('access_token');

  if (access_token) {
    saveTokenData(access_token);
  }
}