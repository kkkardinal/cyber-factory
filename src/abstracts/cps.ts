import { Devices } from '@/abstracts/devices';

export interface Cps {
    id: number,
    createdDate: Date,
    name: string,
    description: string,
    devices: Devices[]
}