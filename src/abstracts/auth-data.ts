export interface AuthData {
    token: string,
    tokenExp: number,
}