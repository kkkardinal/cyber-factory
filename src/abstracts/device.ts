export interface Device {
    id: number;
    name: string;
    ipAddress: string;
    macAddress: string;
    networkInterface: string;
}