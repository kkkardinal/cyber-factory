export interface JwtDecryption {
    payload: string,
    iat: number,
    exp: number
}