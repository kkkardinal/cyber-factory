export interface Devices {
    id: number,
    name: string,
    ipAddress: string,
    macAddress: string,
    networkInterface: string
}