export interface RegisterData {
    email: string;
    password: string;
    role: string;
}