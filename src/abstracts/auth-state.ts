import { AuthData } from '../abstracts/auth-data';

export interface AuthState {
    authData: AuthData,
    loginStatus: string
}