export interface AuthFormData {
    email: string,
    password: string
}

export interface RegisterFormData {
    email: string,
    password: string,
    repeatedPassword: string
}