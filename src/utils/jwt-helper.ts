import * as jwt from 'jsonwebtoken';
import { JwtDecryption } from '@/abstracts/jwt-decryption';

export const jwtDecrypt = (token: string) => {
    return jwt.decode(token) as JwtDecryption;
}

export function tokenAlive(exp: number): boolean {
    return Date.now() < exp * 1000;
}
