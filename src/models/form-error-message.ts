export const EMPTY_FORM_MESSAGE = 'Для отправки формы нужно заполнить обязательные поля и согласиться с условиями';

export const INVALID_CONFIRMATION_MESSAGE = 'Для отправки формы нужно согласиться с условиями';

export const INVALID_FORM_MESSAGE = 'Для отправки формы нужно исправить ошибки в полях';
