import { MenuLink } from '@/abstracts/menu-link';

export const headerMenuLink: MenuLink[] = [
    {
        name: 'Главная',
        link: '/main'
    },
    {
        name: 'Анализ',
        link: '/analyze'
    },
    {
        name: 'Профиль',
        link: '/profile'
    },
]