import { AuthFormData, RegisterFormData } from '@/abstracts/form-data';

export const authFormData: AuthFormData = {
    email: '',
    password: ''
};

export const registerFormData: RegisterFormData = {
    email: '',
    password: '',
    repeatedPassword: '',
}