import axios from 'axios';

export class ApiRequest {
    private url: string = 'http://localhost:5000/api/v1/';
    private api= axios.create({
        url: this.url,
        withCredentials: true,
        headers: {
            "Content-type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('access_token')}`,
        },
    });

    public get(path: string) {

        return this.api
            .get(this.url + path)
            .then((data: any) => data);
    }

    public async post<T>(path: string, data: T) {
        return this.api
            .post(this.url + path, data)
            .then(data => data);
    }

    public async put<T>(path: string, data: T) {
        return this.api.put(this.url + path, data)
    }

    public async delete(path: string) {
        return this.api.delete(this.url + path);
    }
}